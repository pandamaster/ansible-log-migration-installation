Log Migration
=========

Installs the log migration script. 

It compresses the log files to a different folder.
Then uploads the file to Azure
Upon successful upload it deletes the files locally.

As a backup plan, if it fails connecting to Azure
it keeps the compressed file locally but deletes the original file.


Requirements
------------

 <ul>
   <li> PHP 5.6 </li>
   <li> Apache </li>
   <li> PHP Zip extension </li>
 </ul>


Role Variables
--------------
It's best to store the variables in the public group folder

<ul>
<li> branch: master </li
<li> log_migration_script_directory: /var/www/html/pdo/ </li>
<li> azure_connection_string: "qwe" </li>
</ul>


Dependencies
------------

<a href="https://bitbucket.org/pandamaster/usergroups/src/master/"> User Group Role</a>

Example Playbook
----------------


    - hosts: servers
      roles:
         - usergroups
         - log-migration

Other Notes
-------
It was not developed in TDD. But I did tested it across new instances and it works perfectly well.
